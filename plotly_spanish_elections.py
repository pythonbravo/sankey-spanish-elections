from plotly.offline import plot
import pandas as pd

# 1. Load Data

elecciones = pd.read_csv("elecciones.csv").fillna(0) # load only a few data

# 2. Manipulate data: Create lighter colors for links in diagram

def make_rgb_transparent(rgb, bg_rgb, alpha):
    return [alpha * c1 + (1 - alpha) * c2
            for (c1, c2) in zip(rgb, bg_rgb)]
    
elecciones['link color'].replace(regex=True,inplace=True,to_replace=r'rgb',value=r'')
elecciones['link color'].replace(regex=True,inplace=True,to_replace=r'\(',value=r'')
elecciones['link color'].replace(regex=True,inplace=True,to_replace=r'\)',value=r'')

for i in range(len(elecciones['link color'])):
    my_list = elecciones['link color'][i].split(",")
    my_list = map(int,my_list)
    my_list = make_rgb_transparent(my_list,(255,255,255),0.6)
    elecciones.at[i, 'link color']=str('rgb('+str(my_list[0])+','+str(my_list[1])+','+str(my_list[2])+')')

# 3. Sankey Diagram Description

data = dict(
    type='sankey',
    node = dict(
      pad = 15,
      thickness = 10,
      line = dict(
        color = "black",
        width = 0.001
      ),
      label = elecciones['label'][:35],
      color = elecciones['color'][:35]
    ),
    link = dict(
      source = elecciones['source'],
      target = elecciones['target'],
      value = elecciones['value'],
      color = elecciones['link color']
  ))

layout =  dict(
    title = "Elecciones Generales España                     28 Abril 2019                                   10 Noviembre 2019",
    font = dict(
      size = 12
    ),
        annotations=[
        dict(
            x=0.05,
            y=0.92,
            text='Author: https://www.linkedin.com/in/javierbravo/',
            showarrow=False
        )
    ]
    
)

# 4. Plot Data

fig = dict(data=[data], layout=layout)
plot(fig)
